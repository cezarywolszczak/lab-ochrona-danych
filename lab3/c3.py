from Crypto.Cipher import ARC4
from sys import argv
import re

myFile = open('cipher','w')

key = argv[1]
obj1 = ARC4.new(key)
obj2 = ARC4.new(key)

text = re.sub(r'\]', '', str(argv[2:]))
text = re.sub(r'\[', '', text)
text = re.sub(r'\,', '', text)
text = re.sub(r'\'', '', text)

cipher_text = obj1.encrypt(text)
print(text)
print(str("%r" % cipher_text))
myFile.write("%r" % cipher_text)
myFile.close()
obj2.decrypt(cipher_text)

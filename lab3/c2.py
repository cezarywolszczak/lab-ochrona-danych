def run_rc4(k, text):
    cipher_chars = []
    random_byte_gen = pseudo_random_generation_algorithm(k)
    for char in text:
        byte = ord(char)
        cipher_byte = byte ^ random_byte_gen.next()
        cipher_chars.append(chr(cipher_byte))
    return ''.join(cipher_chars)


def key_scheduling_algorithm(key):
    key_length = len(key)
    S = range(256)
    j = 0
    for i in range(256):
        j = (j + S[i] + key[i % key_length]) % 256
        S[i], S[j] = S[j], S[i]
    return S


def gen_counter():
    i = 0
    while True:
        yield i
        i += 1


def pseudo_random_generation_algorithm(S):
    i = 0
    j = 0
    while True:
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        S[i], S[j] = S[j], S[i]

        K = S[(S[i] + S[j]) % 256]
        yield K


def encrypt(k, text):
    while True:
        k_copy = list(k)
        return repr(run_rc4(k_copy, text))


def convert_key(s):
    return [ord(c) for c in s]


if __name__ == '__main__':
    key = 'klucz'
    plain_text = 'text'

    key = convert_key(key)
    key = key_scheduling_algorithm(key) #initial
    print(encrypt(key, plain_text))

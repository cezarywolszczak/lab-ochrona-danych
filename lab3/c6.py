import math
import re
import sys
import c7


def product(*args, **kwds):
    pools = map(tuple, args) * kwds.get('repeat', 1)
    result = [[]]
    for pool in pools:
        result = [x+[y] for x in result for y in pool]
    for prod in result:
        yield tuple(prod)


def attack(key_lenght, text):
    key_yield = length_of_key(key_lenght)
    key_string = ''
    key_entropy = pow(26,key_lenght)
    text_cript = ''
    text_entropy = ''
    text_decrypted = ''
    for i in range(pow(26,key_lenght)):
        key_string = re.sub('[^\w]+', '',str(key_yield.next()))
        #print key_string

        key_string = c7.convert_key(key_string)
        key_string = c7.key_scheduling_algorithm(key_string) #initial
        text_cript = c7.encrypt(key_string, text)
        text_entropy = entropy(text_cript)

        if key_entropy > text_entropy:
            key_entropy = text_entropy
            text_decrypted = text_cript
    print "\ntext wlasciwy:\n \"%s\"" % text_decrypted
    print "entropia: %f" % key_entropy
    return text_decrypted


def length_of_key(len):
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    if len == 1:
        return product(alphabet)
    elif len == 2:
        return product(alphabet, alphabet)
    elif len == 3:
        return product(alphabet, alphabet, alphabet)
    elif len == 4:
        return product(alphabet, alphabet, alphabet, alphabet)
    elif len == 5:
        return product(alphabet, alphabet, alphabet, alphabet, alphabet)
    elif len == 7:
        return product(alphabet, alphabet, alphabet, alphabet, alphabet, alphabet)
    elif len == 8:
        return product(alphabet, alphabet, alphabet, alphabet, alphabet, alphabet, alphabet)
    elif len == 9:
        return product(alphabet, alphabet, alphabet, alphabet, alphabet, alphabet, alphabet, alphabet)
    elif len == 10:
        return product(alphabet, alphabet, alphabet, alphabet, alphabet, alphabet, alphabet, alphabet, alphabet)


def entropy(text):
    stat = {}
#    text= re.sub(r'\s', '', text)
    for sign in text:
        if sign in stat:
            stat[sign] += 1
        else:
            stat[sign] = 1

    entropy = 0
    for i in stat:
        entropy -= stat[i] * math.log(stat[i], 2)
    return -entropy

if __name__ == '__main__':
#    text = str("%r" % sys.argv[2])
    key_lenght = int(sys.argv[1])
    text = "\xee\xf8\xa7*\xca_\x92\xd3\xcd\xee2\x1b\xc3h\x8e\xa6Q\xd0\xa3bRM\xdb\tr\xc7`\xcer>O"
    print str("kryptogram:\n%r" % text), "\nentropia: ", entropy(text)
    attack(key_lenght, text)

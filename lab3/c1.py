from __future__ import generators
import sys


def key_scheduling_algorithm(key):
    key_length = len(key)
    S = range(256)
    j = 0
    for i in range(256):
        j = (j + S[i] + key[i % key_length]) % 256
        S[i], S[j] = S[j], S[i]
    return S


def rc4(key):
    S = key_scheduling_algorithm(key)
    return pseudo_random_generation_algorithm(S)


def pseudo_random_generation_algorithm(S):
    i = 0
    j = 0
    while True:
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        S[i], S[j] = S[j], S[i]

        K = S[(S[i] + S[j]) % 256]
        yield K


def convert_key(s):
    return [ord(c) for c in s]


if __name__ == '__main__':
    key = 'klucz'
    plain_text = 'text'
    gen = ''
    key = convert_key(key)

    key_stream = rc4(key)
    print key_stream

    for i in plain_text:
        gen += "%02X" % (ord(i) ^ key_stream.next())
    print gen
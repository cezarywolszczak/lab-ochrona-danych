import math
import re


def entropy(text):
    stat = {}
    text= re.sub(r'\s', '', text)
    for sign in text:
        if sign in stat:
            stat[sign] += 1
        else:
            stat[sign] = 1

    entropy = 0
    for i in stat:
        print "%c %f %f" % (i,stat[i] , entropy)
        entropy -= stat[i] * math.log(stat[i], 2)
    # print(stat['l'])
    return -entropy


print(entropy('hellodmaskota'))
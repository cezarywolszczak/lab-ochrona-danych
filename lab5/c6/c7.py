import math
import re
import sys


def run_rc4(k, text):
    cipher_chars = []
    random_byte_gen = pseudo_random_generation_algorithm(k)
    for char in text:
        byte = ord(char)
        cipher_byte = byte ^ random_byte_gen.next()
        cipher_chars.append(chr(cipher_byte))
    return ''.join(cipher_chars)


# def rc4(key, plain_text):
#     S = key_scheduling_algorithm(key)
#     key_steam = pseudo_random_generation_algorithm(S)
#     gen = ''
#     for i in plain_text:
#         gen += "%02X" % (ord(i) ^ key_steam.next())
#     return gen


def key_scheduling_algorithm(key):
    key_length = len(key)
    S = range(256)
    j = 0
    for i in range(256):
        j = (j + S[i] + key[i % key_length]) % 256
        S[i], S[j] = S[j], S[i]
    return S


def gen_counter():
    i = 0
    while True:
        yield i
        i += 1


def pseudo_random_generation_algorithm(S):
    i = 0
    j = 0
    while True:
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        S[i], S[j] = S[j], S[i]

        K = S[(S[i] + S[j]) % 256]
        yield K


def encrypt(k, text):
    while True:
        k_copy = list(k)
        return repr(run_rc4(k_copy, text))


def convert_key(s):
    return [ord(c) for c in s]


def entropy(text):
    stat = {}
#    text= re.sub(r'\s', '', text)
    for sign in text:
        if sign in stat:
            stat[sign] += 1
        else:
            stat[sign] = 1

    entropy = 0
    for i in stat:
#        print "%c %f %f" % (i,stat[i] , entropy)
        entropy -= stat[i] * math.log(stat[i], 2)
    # print(stat['l'])
    return -entropy


if __name__ == '__main__':
    key = sys.argv[1]
    plain_text = re.sub(r'\]', '', str(sys.argv[2:]))
    plain_text = re.sub(r'\[', '', plain_text)
    plain_text = re.sub(r'\,', '', plain_text)
    plain_text = re.sub(r'\'', '', plain_text)
    key2 = 'key'
    plain_text2 = 'ala ma kota i psa ale tez ma i kanarka a w ogole to haha i tyle powiem'

    key = convert_key(key)
    key = key_scheduling_algorithm(key) #initial
    cript = encrypt(key, plain_text)
    # cript2 = rc4(key, plain_text)
    print "text wlasciwy:\n\"%s\"\n" % (plain_text)
    print "kryptogram:\n%s \n" % (cript)
    # print "kryptogram: %s \nentropia:%f" % (cript2, entropy(cript2))




import math
import re
import string
import sys

import time

import decimal

import c7


def product(*args, **kwds):
    pools = map(tuple, args) * kwds.get('repeat', 1)
    result = [[]]
    for pool in pools:
        result = [x+[y] for x in result for y in pool]
    for prod in result:
        yield tuple(prod)


def attack(key_lenght, text):
    key_yield = length_of_key(key_lenght)
    key_string = ''
    key_entropy = pow(52,key_lenght)
    text_cript = ''
    text_entropy = ''
    text_decrypted = ''
    for i in range(pow(52,key_lenght)):
        key_string = re.sub('[^\w]+', '',str(key_yield.next()))
        #print key_string

        key_string = c7.convert_key(key_string)
        key_string = c7.key_scheduling_algorithm(key_string) #initial
        text_cript = c7.encrypt(key_string, text)
        text_entropy = entropy(text_cript)

        if key_entropy > text_entropy:
            key_entropy = text_entropy
            text_decrypted = text_cript
    print "\ntext wlasciwy:\n \"%s\"" % text_decrypted
    print "entropia: %f" % key_entropy
    return text_decrypted


def length_of_key(len):
    alphabet = string.ascii_letters
    if len == 1:
        return product(alphabet)
    elif len == 2:
        return product(alphabet, alphabet)
    elif len == 3:
        return product(alphabet, alphabet, alphabet)
    elif len == 4:
        return product(alphabet, alphabet, alphabet, alphabet)
    elif len == 5:
        return product(alphabet, alphabet, alphabet, alphabet, alphabet)
    elif len == 7:
        return product(alphabet, alphabet, alphabet, alphabet, alphabet, alphabet)
    elif len == 8:
        return product(alphabet, alphabet, alphabet, alphabet, alphabet, alphabet, alphabet)
    elif len == 9:
        return product(alphabet, alphabet, alphabet, alphabet, alphabet, alphabet, alphabet, alphabet)
    elif len == 10:
        return product(alphabet, alphabet, alphabet, alphabet, alphabet, alphabet, alphabet, alphabet, alphabet)


def entropy(text):
    stat = {}
#    text= re.sub(r'\s', '', text)
    for sign in text:
        if sign in stat:
            stat[sign] += 1
        else:
            stat[sign] = 1

    entropy = 0
    for i in stat:
        entropy -= stat[i] * math.log(stat[i], 2)
    return -entropy

if __name__ == '__main__':
    time1 = time.time()
    for i in range(1000):
        j = 0
        j += j
    time2 = time.time() - time1
#    text = str("%r" % sys.argv[2])
    key_lenght = int(sys.argv[1])
    text = "j\x00U\xcdI\xee[!'?i[\x7f}\x7f\x9c\xaf\x04\x9e\x07\x8b\x88\xc3\x9cY\x80\x88\xd7\\\x9f\x01\xfdnX"
    print str("kryptogram:\n%r" % text), "\nentropia: ", entropy(text)

    time_str = '%s' % time2
    print (decimal.Decimal(time_str) * pow(52,key_lenght))

    attack(key_lenght, text)

    print '%s' % (time.time() - time1)


from random import randint


def trivial_hash(data):
    hashed_data = 0
    for char in data:
        hashed_data += ord(char)
    return hashed_data % 999


data2 = ' '
data = 'ala ma kota i pas'
while trivial_hash(data) != trivial_hash(data2):
    rand = randint(0, 1)
    if rand == 0:
        abc = ' '
    else:
        abc = '\t'
    data2 += abc

print data
print data2

print trivial_hash(data)
print trivial_hash(data2)

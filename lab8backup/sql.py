from sqlite3 import *
db = connect('secrets.db')
cur = db.cursor()
cur.execute("CREATE TABLE secrets(key TEXT, secret TEXT)")
cur.execute("INSERT INTO secrets VALUES('1234', 'Hello world')")
cur.execute("INSERT INTO secrets VALUES('qwerty', 'Top secret')")
db.commit()
db.close()

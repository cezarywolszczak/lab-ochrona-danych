from random import randint
import sys
import re


def is_primary(n):
    if n == 1:
        return False
    elif n == 2:
        return False
    i = 2
    for i in range(2, n):
        if (n % i == 0):
            return False
    return True


def NWD(a, b):
    while b != 0:
        c = a % b
        a = b
        b = c
    return a

def multiplicative_inverse(e, n2):
    d = 0
    x1 = 0
    x2 = 1
    y1 = 1
    temp_phi = n2

    while e > 0:
        temp1 = temp_phi/e
        temp2 = temp_phi - temp1 * e
        temp_phi = e
        e = temp2

        x = x2- temp1* x1
        y = d - temp1 * y1

        x2 = x1
        x1 = x
        d = y1
        y1 = y

    if temp_phi == 1:
        return d + n2

def generate_keys():
    p = 1
    q = 1
    while not is_primary(p):
        p = randint(1, 999)

    while not is_primary(q) and q != p:
        q = randint(100, 999)

    n = p * q
    n2 = (p - 1) * (q - 1)

    e = 1
    while not is_primary(e):
        e = randint(1, (p - 1) * (q - 1))

    d = multiplicative_inverse(e, n2)

    print "p:", '\033[1;34m%d\033[1;m' % p
    print "q:", '\033[1;34m%d\033[1;m' % q
    print "e:", '\033[1;34m%d\033[1;m' % e
    print "n:", '\033[1;34m%d\033[1;m' % n
    print "d:", '\033[1;34m%d\033[1;m' % d
    print
    return ((e, n), (d, n))

def encrypt(private_key, plaintext):
    key, n = private_key
    cipher = [(ord(char) ** key) % n for char in plaintext]
    return cipher

def decrypt(public_key, ciphertext):
    key, n = public_key
    plain = [chr((char ** key) % n) for char in ciphertext]
    return ''.join(plain)

if __name__ == '__main__':
    plain_text = re.sub(r'\]', '', str(sys.argv[1:]))
    plain_text = re.sub(r'\[', '', plain_text)
    plain_text = re.sub(r'\,', '', plain_text)
    plain_text = re.sub(r'\'', '', plain_text)
    print "Tekst wejsciowy:"
    print '\033[1;34m%s\033[1;m\n' % plain_text
    print "Wlasnosci klucza:"
    private_key, public_key = generate_keys()
    encrypt_text = encrypt(private_key, plain_text)
    encrypt_text2 = ''
    print "Klucz prywatny (d, n):\n", '\033[1;34m(%d, %d)\033[1;m' % (private_key[0], private_key[1])
    print "Klucz publiczny(e, n):\n", '\033[1;34m(%d, %d)\033[1;m\n' % (public_key[0], public_key[1])
    print "Zaszyfrowana wiadomosc:"
    for i in encrypt_text:
        encrypt_text2 += str(i)
    print '\033[1;34m%s\033[1;m' % encrypt_text2
    decrypt_text = decrypt(public_key, encrypt_text)
    print "\nOdszyfrowana wiadomosc:"
    print '\033[1;34m%s\033[1;m\n' % decrypt_text

# plain_text = "anna"
# num_text = []
# cript_text = []
# j = 2
# tmp = ''
# plain_text = plain_text + (j-(len(plain_text) % j)) * ' '
# print plain_text
#
# #postac liczbowa TODO
# for i in range(0,len(plain_text)-j,j):
#     for k in range(0,j):
#         tmp += '%s' % ord(plain_text[i+k])
#     num_text.append(int(tmp))
#     tmp = ''
# print num_text
#
# print 'Tekst w postaci zaszyfrowanej: '
# for i in num_text:
#     cript_text.append(pow(i,e) % n)
#     sys.stdout.write('%d ' % (pow(i,e) % n))
#
# #---- d->e ----#
#
# print '\nTekst w postaci jawnej(liczbowej): '
# unciper_text = []
# for i in cript_text:
#     unciper_text.append(pow(i,d) % n)
#     sys.stdout.write('%d ' % (pow(i,d) % n))

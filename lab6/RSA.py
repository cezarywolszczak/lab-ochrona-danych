from Crypto.PublicKey import RSA
import sys
import re

plain_text = re.sub(r'\]', '', str(sys.argv[1:]))
plain_text = re.sub(r'\[', '', plain_text)
plain_text = re.sub(r'\,', '', plain_text)
plain_text = re.sub(r'\'', '', plain_text)

rsa_keys = RSA.generate(1024)

pub_key = rsa_keys.publickey()

encrypted = pub_key.encrypt(plain_text, "some randomness")

print '\033[1;34m%s\033[1;m\n' % (("%s" % encrypted).encode('hex'))
print '\033[1;34m%s\033[1;m\n' % rsa_keys.decrypt(encrypted)

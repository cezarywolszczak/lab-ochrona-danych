# coding=utf-8
import string
import sys
from random import randint


def create_key(key, bits):
    rand = randint(0, 25)
    # sól początkowa litera = liczba przesunięcia hasła do przodu
    salt1 = chr(ord('a') + rand)
    crypto_key = salt1
    # druga litera = długość soli końcowejz
    crypto_key = crypto_key + chr(bits - ((len(key) + 2) % 16) + ord('a') - 1 )  # chr(ord('a') + len(key))
    print "sol poczatkowa:", crypto_key
    salt2 = ''
    for i in range((bits - (len(key)+2) % 16)):
        salt2 = salt2 + chr(ord('a') + randint(0, 25))
    print "sol koncowa:", salt2

    alphabet = ''
    back_to = 0
    back_to2 = 26

    numer_of = rand
    for x in range(26):
        if numer_of >= 0:
            alphabet = alphabet + chr(ord('a') + numer_of + x - back_to)
            if (ord('a') + numer_of + x + 1 - back_to) > 122:
                back_to = 26
        elif numer_of < 0:
            alphabet = alphabet + chr(ord('a') + numer_of + x + back_to2)
            if (ord('a') + numer_of + x + 1 + back_to2) > 122:
                back_to2 = 0

    table = string.maketrans("abcdefghijklmnopqrstuvwxyz", alphabet)

    line = key.rstrip()
    print "przesuniente haslo:", (string.translate(line, table))
    crypto_key = crypto_key + string.translate(line, table)
    crypto_key = crypto_key + salt2

    print "klucz: %r" % key
    print "klucz zaszyfrowany: %r" % crypto_key  # .encode('hex')
    print "klucz zaszyfrowany hex: %r" % crypto_key.encode('hex')

#klucz 13 bitów!!!
if __name__ == '__main__':
    key = sys.argv[1]
    create_key(key, 16)

import math
import sys
import re


def entropy(text):
    stat = {}
#    text= re.sub(r'\s', '', text)
    for sign in text:
        if sign in stat:
            stat[sign] += 1
        else:
            stat[sign] = 1

    entropy = 0
    for i in stat:
        entropy -= stat[i] * math.log(stat[i], 2)
    return -entropy


def entropy2(text):
    entropy2 = len(text) * math.log(100, 2)
    return entropy2


if __name__ == '__main__':
#    key = sys.argv[1]
    key =  ""
    text = re.sub(r'\]', '', str(sys.argv[2:]))
    text = re.sub(r'\[', '', text)
    text = re.sub(r'\,', '', text)
    text = re.sub(r'\'', '', text)

#    print "Tekst czysty-entropia:", entropy(text)
    while entropy2(key) < 256:
	key = key + 'a'   
	if entropy2(key + "a") > 256:
	    break
   
    print "Klucz czysty-entropia:", entropy2(key)
    print "Klucz dlugosc", len(key)

import math
import sys
import re
from Crypto.Cipher import DES, AES
from Crypto import Random


def entropy(text):
    stat = {}
#    text= re.sub(r'\s', '', text)
    for sign in text:
        if sign in stat:
            stat[sign] += 1
        else:
            stat[sign] = 1

    entropy = 0
    for i in stat:
        entropy -= stat[i] * math.log(stat[i], 2)
    return -entropy


def entropy2(text):
    entropy2 = len(text) * math.log(26, 2)
    return entropy2


if __name__ == '__main__':
    key = sys.argv[1]

    bits = 16
    key = key + (bits - (len(key)) % 16) * 'a'

    text = re.sub(r'\]', '', str(sys.argv[2:]))
    text = re.sub(r'\[', '', text)
    text = re.sub(r'\,', '', text)
    text = re.sub(r'\'', '', text)


    # iv = Random.new().read(DES.block_size)
    # des = DES.new("key12345")
    # des = DES.new("key12345", DES.MODE_CBC, iv)
    # encrypted = des.encrypt("secret12")
    # print encrypted.encode('hex')

    text = text + (bits - (len(text)) % 16) * 'a'
    obj = AES.new(key, AES.MODE_ECB)
    ciphertext = obj.encrypt(text)

    encryptor = AES.new(key, AES.MODE_ECB, 'This is an IV456')
    u_ciphertext = encryptor.decrypt(ciphertext)

    print
    print "Teks czysty:", text
    print "Tekst czysty-entropia:", entropy(text)
#    print "Klucz czysty-entropia:", entropy2(key)
    print ""
    print "Tekst-zaszyfrowany:", str("%r" % ciphertext)
    print "Tekst-zaszyfrowany-hex:", ciphertext.encode('hex')
    print "Tekst zaszyfrowany-entropia:", entropy(text.encode('hex'))
#    print "Klucz zaszyfrowany-entropia:", entropy2(key)
    print "Tekst rozszyfrowany: %r" % u_ciphertext

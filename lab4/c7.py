import math
import re
import sys
from Crypto.Cipher import AES

import c7


def product(*args, **kwds):
    pools = map(tuple, args) * kwds.get('repeat', 1)
    result = [[]]
    for pool in pools:
        result = [x+[y] for x in result for y in pool]
    for prod in result:
        yield tuple(prod)


def attack(key_lenght, text):
    key_yield = length_of_key(key_lenght)
    key_string = ''
    key_entropy = pow(26,key_lenght)
    text_cript = ''
    text_entropy = ''
    text_decrypted = ''
    for i in range(pow(26, key_lenght)):
        key_string = re.sub('[^\w]+', '',str(key_yield.next())) + (16 - key_lenght) * 'a'
        #print key_string

        # key_string = c7.convert_key(key_string)
        # key_string = c7.key_scheduling_algorithm(key_string) #initial
        # text_cript = c7.encrypt(key_string, text)
        # text_entropy = entropy(text_cript)

        encryptor = AES.new(key_string, AES.MODE_CBC, 'This is an IV456')
        text_cript = encryptor.decrypt(text)
        text_entropy = entropy("%r" % text_cript)
        # print "%r %f" % (text_cript, text_entropy)

        if key_entropy > text_entropy:
            key_entropy = text_entropy
            text_decrypted = text_cript
    print "\ntext wlasciwy:\n \"%r\"" % text_decrypted
    print "entropia: %f" % key_entropy
    return text_decrypted


def length_of_key(len):
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    if len == 1:
        return product(alphabet)
    elif len == 2:
        return product(alphabet, alphabet)
    elif len == 3:
        return product(alphabet, alphabet, alphabet)
    elif len == 4:
        return product(alphabet, alphabet, alphabet, alphabet)
    elif len == 5:
        return product(alphabet, alphabet, alphabet, alphabet, alphabet)


def entropy(text):
    stat = {}
#    text= re.sub(r'\s', '', text)
    for sign in text:
        if sign in stat:
            stat[sign] += 1
        else:
            stat[sign] = 1

    entropy = 0
    for i in stat:
        entropy -= stat[i] * math.log(stat[i], 2)
    return -entropy

def entropy2(text):
    entropy2 = len(text) * math.log(26, 2)
    return entropy2

if __name__ == '__main__':
#    text = str("%r" % sys.argv[2])
    key_lenght = int(sys.argv[1])
    text = "\xbb\r\x8f\x874L\xbc\x08\x1f\x15'w\xaf0\n\x12w\xf86\xf2\x02w\n\xb4\x86R)\x11\x89\x7f\xec\xaf\xf5\x0e\xcd\xe2\x0c\xb7:%\x1d0@\x01\\\x1e\x0b;\xfc\xfb_\x06B\xd3xij\xfe\xf3\x10\xc4\xa8\x8aO"
    print str("kryptogram:\n%r" % text), "\nentropia: ", entropy(text)
    attack(key_lenght, text)
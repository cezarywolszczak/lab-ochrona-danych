from Crypto.Cipher import DES, AES
from Crypto import Random


iv = Random.new().read(DES.block_size)
print iv
des = DES.new("key12345")
des = DES.new("key12345", DES.MODE_CBC, iv)
encrypted = des.encrypt("secret12")
print encrypted
aes = AES.new("1234567890123456", AES.MODE_CFB, 'This is an IV456')
encrypted = aes.encrypt("test")
print encrypted

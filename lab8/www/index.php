<?php
session_start();
if (isset($_SESSION['log_in']) && $_SESSION['log_in'] == true) {
    header('Location: game/main_menu.php');
    exit();
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Logowanie</title>


    <script src="" type="text/javascript"></script>
    <link rel="stylesheet" href="css/style.css" type="text/css"/>

</head>
<body>

<div id="container">
    <div id="log">
        <form action="log/login.php" method="post">
            <br>Login:
            <input id="login" type="text" name="login" title="Login">
            <br><br>Hasło:
            <input id="password" type="password" name="password" title="Hasło">
            <br><br>
            <input type="submit" value="Zaloguj">
            <br><br>
            Nie masz jeszcze konta? <a href="log/register.php">Zarejestruj się</a><br>
            Nie pamiętasz hasła <a href="log/password_recovery.php">Odzyskaj hasło</a>
        </form>
        <br>
        <?php
        if (isset($_SESSION['error'])) {
            echo $_SESSION['error'];
            unset($_SESSION['error']);
        }
        ?>
    </div>
    <div style="clear: both;"></div>
</div>

</body>

</html>
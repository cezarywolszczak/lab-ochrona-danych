<?php
session_start();
if (!isset($_SESSION['log_in'])) {
    header('Location: index.php');
    exit();
}

if (isset($_POST['password'])) {
    $validation = true;

    $old_password = $_POST['password'];
//validation of password
    $new_password = $_POST['new_password'];
    $password_repeat = $_POST['password_repeat'];
    if ((strlen($new_password) < 8) || (strlen($new_password) > 26)) {
        $validation = false;
        $_SESSION['e_password'] = "Hasło musi mieć od 8 do 26 znaków";
    }
    if ($new_password != $password_repeat) {
        $validation = false;
        $_SESSION['e_password_repeat'] = "Podane hasła nie są identyczne";
    }
    $password_hash = password_hash($new_password, PASSWORD_DEFAULT);


    require_once "log/connect.php";
    mysqli_report(MYSQLI_REPORT_STRICT); //raportowanie błędów oparte o wyjątki
    try {
        $connection = new mysqli($host, $db_user, $db_password, $db_name);
        if ($connection->connect_errno != 0) {
            throw new Exception(mysqli_connect_errno());
        } else {
            if ($validation == true) {
                //is nick exist
                if ($result = $connection->query(sprintf("SELECT * FROM users WHERE user='%s'",
                    $_SESSION['user']))
                ) {
                    $row = $result->fetch_assoc();
                    if (password_verify($old_password, $row['password'])) {
                        $id = $row['id'];
                        $connection->query("UPDATE users SET password='$password_hash' WHERE id='$id'");
                    } else {
                        $_SESSION['error'] = "Zle hasło";
                    }
                }
            }
            $connection->close();
        }
    } catch (Exception $e) {
        echo "Błąd serwera prosimy zarejestrować się w innym terminie<br>";
        echo "Info o błędzie: " . $e;
    }
}
?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Menu główne</title>
    <link rel="icon" href="">


    <link rel="stylesheet" href="css/style.css" type="text/css"/>

</head>

<body>

<div id="second_menu">
    <div class="second_menu_items"><?php
        echo "User: &nbsp;&nbsp;" . @$_SESSION['user'];
        ?>
    </div>
    <div class="second_menu_items" style="width: 42%;">
        &nbsp;
    </div>
    <div class="second_menu_items" id="timer"></div>
    <div class="second_menu_items">
        <a href="main_menu.php">Menu główne</a>
    </div>
    <div class="second_menu_items">
        <a href="log/logout.php">Wyloguj</a>
    </div>
</div>

<div style="text-align: center; margin-top: 50px; font-size: 30px">
    Triter ustawienia
</div>

<div id="container">
    <div id="log">
        <form action="settings.php" method="post">
            <br>Stare hasło:
            <input id="password" type="password" name="password" title="Nowe hasło">
            <br>
            <?php
            if (isset($_SESSION['error'])) {
                echo $_SESSION['error'];
                unset($_SESSION['error']);
            }
            ?>
            <br>
            <br>Nowe hasło:
            <input id="new_password" type="password" name="new_password" title="Nowe hasło">
            <?php
            if (isset($_SESSION['e_password'])) {
                echo '<div class="error">' . $_SESSION['e_password'] . '</div>';
                unset($_SESSION['e_password']);
            }
            ?>
            <br><br>Powtórz hasło:
            <input id="password_repeat" type="password" name="password_repeat" title="Powtórz hasło">
            <br>
            <?php
            if (isset($_SESSION['e_password_repeat'])) {
                echo '<div class="error">' . $_SESSION['e_password_repeat'] . '</div>';
                unset($_SESSION['e_password_repeat']);
            }
            ?>

            <br><br>
            <input type="submit" value="Ok">
            <br><br>
        </form>
        <br>

    </div>
</div>

<div id="container">

</div>


</body>

</html>
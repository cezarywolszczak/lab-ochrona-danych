<?php
session_start();

if (isset($_POST['nick'])) {

    $validation = true;

    //validation of nick
    $nick = $_POST['nick'];
    if ((strlen($nick) < 3) || (strlen($nick) > 23)) {
        $validation = false;
        $_SESSION['e_nick'] = "Nick musi posiadać od 3 do 23 znaków!";
    }
    if (ctype_alnum($nick) == false) { //sprawdzenie czy wszystkie znaki są alfanumeryczne
        $validation = false;
        $_SESSION['e_nick'] = 'Nick może składać się wyłącznie z liter i cyfr (bez polskich znaków)!';
    }
    
    //validation color
    $color = $_POST['color'];
    if ((strlen($color) < 3) || (strlen($color) > 26)) {
        $validation = false;
        $_SESSION['e_color'] = "Ulubiony kolor musi mieć od 3 do 26 znaków";
    }

    
    //remember data_in
    $_SESSION['fr_nick'] = $nick;

   
    require_once "connect.php";
    mysqli_report(MYSQLI_REPORT_STRICT); //raportowanie błędów oparte o wyjątki
    try {
        $connection = new mysqli($host, $db_user, $db_password, $db_name);
        if ($connection->connect_errno != 0) {
            throw new Exception(mysqli_connect_errno());
        } else {

            //is nick exist
            if($result = $connection->query(sprintf("SELECT color FROM users WHERE user='%s'",
                mysqli_real_escape_string($connection, $nick))))
            {
                $row = $result->fetch_assoc();
                if (password_verify($color, $row['color'])) {
                    $_SESSION['nick_and_colour_verify'] = true;
                    header('Location: set_password.php');
                }else
                    $_SESSION['error'] = "Login lub hasło są inne";
            }else
                $_SESSION['error'] = "Login lub hasło są inne";


            $connection->close();
        }
    } catch (Exception $e) {
        echo "Błąd serwera prosimy zarejestrować się w innym terminie<br>";
        echo "Info o błędzie: " . $e;
    }

}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Odzyskaj hasło</title>

    <link rel="stylesheet" href="../css/style.css" type="text/css"/>


</head>
<body>

<div id="register_form">
    <form method="post">
        Nickname:<br>
        <input type="text" name="nick" title="Nickname" value="<?php
        if (isset($_SESSION['fr_nick'])) {
            echo $_SESSION['fr_nick'];
        }
        ?>"><br>
        <?php
        if (isset($_SESSION['e_nick'])) {
            echo '<div class="error">' . $_SESSION['e_nick'] . '</div>';
            unset($_SESSION['e_nick']);
        }
        ?>


        Twój ulubiony kolor:<br>
        <input type="password" name="color" title="Twój ulubiony kolor" value="<?php
        if (isset($_SESSION['fr_color'])) {
            echo $_SESSION['fr_color'];
            unset($_SESSION['fr_color']);
        }
        ?>"><br><br>
        <?php
        if (isset($_SESSION['e_color'])) {
            echo '<div class="error">' . $_SESSION['e_color'] . '</div>';
            unset($_SESSION['e_color']);
        }
        ?>
        <?php
        if(isset($_SESSION['error'])){
            echo $_SESSION['error'];
            unset($_SESSION['error']);
        }
        ?>
        <br>
        <input type="submit" value="Potwierdź">

    </form>
</div>
</body>

</html>
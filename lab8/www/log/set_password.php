<?php
session_start();
if (!isset($_SESSION['nick_and_colour_verify']) || $_SESSION['nick_and_colour_verify'] == false) {
    header('Location: ../index.php');
    exit();
}
if (isset($_SESSION['log_in'])) {
    header('Location: ../index.php');
    exit();
}

$nick = $_SESSION['fr_nick'];

if (isset($_POST['password'])) {
    $validation = true;
//validation of password
    $password = $_POST['password'];
    $password_repeat = $_POST['password_repeat'];
    if ((strlen($password) < 8) || (strlen($password) > 26)) {
        $validation = false;
        $_SESSION['e_password'] = "Hasło musi mieć od 8 do 26 znaków";
    }
    if ($password != $password_repeat) {
        $validation = false;
        $_SESSION['e_password_repeat'] = "Podane hasła nie są identyczne";
    }
    $password_hash = password_hash($password, PASSWORD_DEFAULT);


    require_once "connect.php";
    mysqli_report(MYSQLI_REPORT_STRICT); //raportowanie błędów oparte o wyjątki
    try {
        $connection = new mysqli($host, $db_user, $db_password, $db_name);
        if ($connection->connect_errno != 0) {
            throw new Exception(mysqli_connect_errno());
        } else {
            if ($validation == true) {
                //is nick exist
                if ($result = $connection->query(sprintf("SELECT id FROM users WHERE user='%s'",
                    mysqli_real_escape_string($connection, $nick)))
                ) {

                    $row = $result->fetch_assoc();
                    $id = $row['id'];
                    $connection->query("UPDATE users SET password='$password_hash' WHERE id='$id'");
                    header("Location: ../change_pass_complete.php");

                } else {
                    $_SESSION['error'] = "Login lub hasło są inne";
                }
            }
            $connection->close();
        }
    } catch (Exception $e) {
        echo "Błąd serwera prosimy zarejestrować się w innym terminie<br>";
        echo "Info o błędzie: " . $e;
    }
}
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Zmiana hasła</title>


    <script src="" type="text/javascript"></script>
    <link rel="stylesheet" href="../css/style.css" type="text/css"/>

</head>
<body>

<div id="container">
    <div id="log">
        <form action="set_password.php" method="post">
            <br>Nowe hasło:
            <input id="password" type="password" name="password" title="Nowe hasło">
            <br>
            <?php
            if (isset($_SESSION['e_password'])) {
                echo '<div class="error">' . $_SESSION['e_password'] . '</div>';
                unset($_SESSION['e_password']);
            }
            ?>
            <br><br>Powtórz hasło:
            <input id="password_repeat" type="password" name="password_repeat" title="Powtórz hasło">
            <br>
            <?php
            if (isset($_SESSION['e_password_repeat'])) {
                echo '<div class="error">' . $_SESSION['e_password_repeat'] . '</div>';
                unset($_SESSION['e_password_repeat']);
            }
            ?>
            <br><br>
            <input type="submit" value="Ok">
            <br><br>
        </form>
        <br>

    </div>
</div>

</body>

</html>
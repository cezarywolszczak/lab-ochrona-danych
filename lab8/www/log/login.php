<?php

    @session_start();

    if(!isset($_POST['login']) && !isset($_POST['password'])){
        header('Location: ../index.php');
        exit();
    }

    //implementacja bazy MySQL
    require_once "connect.php";
    $connection = @new mysqli($host,$db_user,$db_password,$db_name);

    //sprawdzenie czy nawiązano połączenie, udane połączenie == 0
    if($connection->connect_errno != 0){
        echo "ERROR".$connection->connect_errno;
    }
    else {
        $login = $_POST['login'];
        $password = $_POST['password'];

        $login = htmlentities($login, ENT_QUOTES,"UTF-8");

        if($result = $connection->query(sprintf("SELECT * FROM users WHERE user='%s'",
            mysqli_real_escape_string($connection, $login))))
        {

            $number_of_users = $result->num_rows;
            if($number_of_users > 0){
                $row = $result->fetch_assoc();
                if(password_verify($password, $row['password'])) {
                    $_SESSION['log_in'] = true;
                    $_SESSION['id'] = $row['id'];
                    $_SESSION['user'] = $row['user'];
                    $_SESSION['email'] = $row['email'];
                    $_SESSION['color'] = $row['color'];


                    unset($_SESSION['error']);
                    $result->close();
                    header("Location: ../main_menu.php");
                } else{
                    $_SESSION['error'] = '<p style="color: red; font-weight: 600">Nieprawidłowy login lub hasło</p>';
                    header('Location: ../index.php');
                }
            }else {
                $_SESSION['error'] = '<p style="color: red; font-weight: 600">Nieprawidłowy login lub hasło</p>';
                header('Location: ../index.php');
            }

        }
        $connection->close();
    }

?>
